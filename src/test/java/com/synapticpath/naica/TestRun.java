/* Copyright (C) 2017 synapticpath.com - All Rights Reserved

 This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.synapticpath.naica;

import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.synapticpath.naica.reports.HtmlFileSystemReport;
import com.synapticpath.naica.selenium.SeleniumTestContext;
import com.synapticpath.naica.selenium.SeleniumTestProperties;
import com.synapticpath.naica.selenium.SeleniumTestBuilder;

/**
 * Before running this test case for the first time, make sure that the 
 * Selenium web driver is properly configured. 
 * 
 * 
 * First, download mozilla gecko driver https://github.com/mozilla/geckodriver/releases 
 * After unpacking the gecko driver, start test with VM options 
 * -Dwebdriver.gecko.driver=../geckodriver
 * 
 * In the past, the VM argument to configure firefox for tests was: -Dwebdriver.firefox.marionette=/usr/bin/firefox or
 * webdriver.gecko.driver=/usr/bin/firefox depending on browser version. 
 * 
 * @author developer@synapticpath.com
 *
 */
public class TestRun {
	
	private SeleniumTestContext context;
	
	@Before
	public void init() {
		FirefoxDriver driver = new FirefoxDriver();
		context = SeleniumTestContext.getInstance();
		context.setDriver(driver);		
	}
	
	@After
	public void finalize() {
		//Context finalizes automatically at the end of the run.
		//context.finalize();
	}
	
	@Ignore
	@Test
	public void runTest() {		
		TestProperties props = getTestProperties();				
		context.setProperties(props);

		createRunner().run();				
		assertFalse(context.isFailed());
	}
	
	@Test
	public void testBuilder() throws Exception {
				
		SeleniumTestBuilder builder = new SeleniumTestBuilder("naica-tests");
		TestSuiteRunner runner = builder.build();
		runner.run();		
		
		assertFalse(context.isFailed());
	}
	
	
	public TestProperties getTestProperties() {
		
		SeleniumTestProperties props = new SeleniumTestProperties("synapticpath-web",
				"Here we put a short description of the tests this instance of TestContext performs. "+
						"This is an optional parameter that will appear in the test report. In this case, "+
						"we are testing some of the UI functionality that is presented at http://synapticpath.com ");
		
		return props;
	}
	
	public TestSuiteRunner createRunner() {
		
		TestCaseRunner caseRunner = new TestCaseRunner("NavigateMainMenu", new TestWeb());
		
		TestSuiteRunner runner = new TestSuiteRunner(caseRunner);
		runner.setReportGenerators(new HtmlFileSystemReport());
		return runner;
	}

}
