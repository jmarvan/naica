package com.synapticpath.naica;

import com.synapticpath.naica.ops.ChainOp;
import com.synapticpath.naica.ops.Op;
import com.synapticpath.naica.selenium.SeleniumAction;
import com.synapticpath.naica.selenium.SeleniumCondition;
import com.synapticpath.naica.selenium.SeleniumOp;
import com.synapticpath.naica.selenium.SeleniumSelector;
import com.synapticpath.naica.selenium.SeleniumSnapOp;

public class TestWeb extends ChainOp {

	public TestWeb() {
		super(new Op[] {

				SeleniumOp.on(SeleniumAction.get("http://synapticpath.com"), SeleniumCondition.url("synapticpath.com"))
						.newStep(true).onSuccess("synapticpath.com main page is visible.")
						.description("Go to synapticpath.com"),

				SeleniumSnapOp.snapOp("products", SeleniumAction.click(SeleniumSelector.byCss("a[href=\"products.html\"]")),
								SeleniumCondition.url("products.html").onSuccess("Products page is visible."))
						.newStep(true).description("Navigate to products page."),

				SeleniumSnapOp.snapOp("services", SeleniumAction.click(SeleniumSelector.byCss("a[href=\"services.html\"]")),
								SeleniumCondition.url("services.html").onSuccess("Services page is visible."))
						.newStep(true).description("Navigate to services page.")

		});

	}

}
