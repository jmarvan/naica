package com.synapticpath.naica.selenium;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.StringTokenizer;

import com.synapticpath.naica.TestCaseRunner;
import com.synapticpath.naica.TestContext;
import com.synapticpath.naica.TestProperties;
import com.synapticpath.naica.TestSuiteRunner;
import com.synapticpath.naica.ops.Op;
import com.synapticpath.naica.reports.HtmlFileSystemReport;
import com.synapticpath.naica.reports.ReportGenerator;

/**
 * Responsible for building a TestSuite using properties configuration file.
 * 
 * @author developer@synapticpath.com
 *
 */
public class SeleniumTestBuilder {
		
	private String name;	
	private Properties props;
	
	public SeleniumTestBuilder(String name) {
		this.name = name;		
		
		try {
			loadProperties();
		} catch (Exception e) {
			throw new IllegalStateException("Problem reading configuration properties.", e);
		}
	}
	
	private void loadProperties() throws FileNotFoundException, IOException {
		props = new Properties();
        props.load(getClass().getResourceAsStream("/"+name+".properties"));
	}
	
	private void loadPropertiesFromFile(String filePath) throws FileNotFoundException, IOException {
		
		props = new Properties();
		
		File file = new File(filePath);
		InputStream inputStream = new FileInputStream(file);
		
		props.load(inputStream);		
	}
	
	public TestSuiteRunner build() throws Exception {
		
		TestContext.getInstance().setProperties(getTestProperties());
		
		TestCaseRunner [] testCases = makeTestCases();
		TestSuiteRunner runner = new TestSuiteRunner(testCases);
		runner.setReportGenerators(getReportGenerators());
		
		return runner;
	}
	
	protected TestCaseRunner [] makeTestCases() throws Exception {
	
		String testCases = props.getProperty("naica-selenium.testCases");
		if (testCases == null) {
			throw new IllegalArgumentException("No testcase definitions found.");
		}
		
		int index = 0;				
		StringTokenizer st = new StringTokenizer(testCases, ",");
		TestCaseRunner [] runners = new TestCaseRunner [st.countTokens()];
		
		while (st.hasMoreTokens()) {
			
			String testCaseId = st.nextToken();
			String testCaseClass = props.getProperty("naica-selenium.testCase."+testCaseId+".className");
			String description = props.getProperty("naica-selenium.testCase."+testCaseId+".description");
			
			Object testOp =  Class.forName(testCaseClass).newInstance();						
			
			runners[index++] = new TestCaseRunner(testCaseId, description, (Op)testOp);
		
		}
		
		return runners;
	}
	
	protected ReportGenerator [] getReportGenerators() throws Exception {
		String generatorNames = props.getProperty("naica-selenium.report.generators");
		
		if (generatorNames != null) {
			
			int index = 0;
			StringTokenizer st = new StringTokenizer(generatorNames, ",");
			ReportGenerator [] reportGenerators = new ReportGenerator [st.countTokens()];
			
			while (st.hasMoreTokens()) {
				
				String generatorId = st.nextToken();
				String generatorClassName = props.getProperty("naica-selenium.report.generator."+generatorId+".className");
				
				ReportGenerator reportGenerator =  (ReportGenerator)Class.forName(generatorClassName).newInstance();
				reportGenerators[index++] = reportGenerator;
			}
			
			return reportGenerators;
		}
				
		return new ReportGenerator [] {new HtmlFileSystemReport()};
	}
	
	
	public TestProperties getTestProperties() {
		
		String testSuiteName = props.getProperty("naica-selenium.testsuite.name", name);
		String description = props.getProperty("naica-selenium.testsuite.description");
		String basePath = props.getProperty("naica-selenium.basePath");
		
		SeleniumTestProperties props = new SeleniumTestProperties(testSuiteName, description, basePath);
		
		return props;
	}

}
