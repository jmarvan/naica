package com.synapticpath.naica.ops;

public class ChainOp implements Op {
	
	private Op [] opChain;
	
	public ChainOp(Op ... ops) {
		this.opChain = ops;
	}

	@Override
	public boolean execute() {
		
		boolean success = true;
		for (Op op: opChain) {
			//TODO implement failFast ... 
			op.execute();
		}
 
		return success;
	}

}
